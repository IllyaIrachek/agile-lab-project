﻿namespace Tunebook.Core
{
    public enum PlayerState
    {
        Stop,
        Play,
        Pause
    }
}