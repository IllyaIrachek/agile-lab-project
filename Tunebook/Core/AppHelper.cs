﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Splat;

namespace Tunebook.Core
{
    public sealed class AppHelper
    {
        private static readonly object lockErrorDialog = 1;
        private static bool blockExceptionDialog;

        public void ConfigureApp(Application app, string appName)
        {
            this.ApplicationStartedTime = DateTime.UtcNow;
            this.ApplicationName = appName;

            // check where we can write
            if (CanCreateFile("."))
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                ApplicationPath = Path.GetDirectoryName(path);
            }
            else
            {
                ApplicationPath = ApplicationDataPath();
            }

            // setup exception handling
            this.ConfigureHandlingUnhandledExceptions();
        }

        public string ApplicationName { get; private set; }

        public string ApplicationPath { get; private set; }

        public DateTime ApplicationStartedTime { get; private set; }

        private void ConfigureHandlingUnhandledExceptions()
        {
            // see more: http://dotnet.dzone.com/news/order-chaos-handling-unhandled
            Thread.GetDomain().UnhandledException += this.BackgroundThreadUnhandledException;
            if (Application.Current != null)
            {
                Application.Current.DispatcherUnhandledException += this.WPFUIThreadException;
            }
        }

        private void BackgroundThreadUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleUnhandledException(e.ExceptionObject, true);
        }

        private void WPFUIThreadException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true; // prevent app to quit automatically, see: http://dotnet.dzone.com/news/order-chaos-handling-unhandled
            HandleUnhandledException(e.Exception, false);
        }

        public void HandleUnhandledException(object exception, bool exitProgram)
        {
            // only allow one thread (UI)
            lock (lockErrorDialog)
            {
                try
                {
                    // check for special exitprogram conditions
                    if (!exitProgram)
                    {
                        exitProgram = ExceptionExtensions.IsFatalException((Exception)exception);
                    }
                    // prevent recursion on the message loop
                    if (!blockExceptionDialog)
                    {
                        blockExceptionDialog = true;
                        try
                        {
                            // we do not switch to the UI thread, because the dialog that we are going to show has its own message pump (all dialogs have). 
                            // As long as the dialog does not call methods of other windows there should be no problem.
                            if (exception == null)
                            {
                                //ShowStackTraceBox("Unhandled Exception Occurred", Environment.StackTrace);
                            }
                            else if (exception is Exception)
                            {
                                //ShowErrorBox((Exception)exception);
                            }
                            else
                            {
                                // won't happen really - exception is really always of type Exception
                                //ShowStackTraceBox("Unhandled Exception Occurred: " + exception, Environment.StackTrace);
                            }
                        }
                        finally
                        {
                            if (!exitProgram)
                            {
                                blockExceptionDialog = false;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    if (exitProgram)
                    {
                        Environment.Exit(0);
                    }
                }
            }
        }

        public static bool CanCreateFile(string dir)
        {
            var file = Path.Combine(dir, Guid.NewGuid().ToString() + ".tmp");
            // perhaps check File.Exists(file), but it would be a long-shot...
            bool canCreate;
            try
            {
                using (File.Create(file)) { }
                File.Delete(file);
                canCreate = true;
            }
            catch
            {
                canCreate = false;
            }
            return canCreate;
        }

        public string ApplicationDataPath()
        {
            var appData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), this.ApplicationName);
            return appData;
        }
    }
}