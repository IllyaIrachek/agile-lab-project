﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime;
using System.Windows;
using ReactiveUI;
using Tunebook.Core;
using Tunebook.Core.Player;
using Tunebook.ViewModels;
using Tunebook.Views;
using Unity;
using Unity.Lifetime;

namespace Tunebook
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static UnityContainer Container { get; private set; }
        private AppHelper appHelper;

        public App()
        {
            InitializeContainer();

            appHelper = Container.Resolve<AppHelper>();
            appHelper.ConfigureApp(this, Assembly.GetExecutingAssembly().GetName().Name);

            // Enable Multi-JIT startup
            var profileRoot = Path.Combine(appHelper.ApplicationPath, "ProfileOptimization");
            Directory.CreateDirectory(profileRoot);
            // Define the folder where to save the profile files
            ProfileOptimization.SetProfileRoot(profileRoot);
            // Start profiling and save it in Startup.profile
            ProfileOptimization.StartProfile("Startup.profile");
        }

        private void InitializeContainer()
        {
            Container = new UnityContainer();

            Container.RegisterType<AppHelper>(new ContainerControlledLifetimeManager());
            Container.RegisterType<PlayerSettings>(new ContainerControlledLifetimeManager());
            Container.RegisterType<PlayerEngine>(new ContainerControlledLifetimeManager());
            Container.RegisterType<MainWindow>();
            Container.RegisterType<IReactiveObject, MainViewModel>();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow = Container.Resolve<MainWindow>();
            MainWindow.Show();
        }
    }
}