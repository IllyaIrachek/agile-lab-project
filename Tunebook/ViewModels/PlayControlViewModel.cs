﻿using System;
using System.Linq;
using System.Reactive;
using System.Windows.Input;
using ReactiveUI;
using Tunebook.Core;
using Tunebook.Core.Player;
using Unity;

namespace Tunebook.ViewModels
{
    public class PlayControlViewModel : ReactiveObject
    {
        private readonly PlayListsViewModel playListsViewModel;
        private ICommand playOrPauseCommand;
        private ICommand stopCommand;
        private ICommand playPrevCommand;
        private ICommand playNextCommand;

        public PlayControlViewModel(MainViewModel mainViewModel)
        {
            this.playListsViewModel = mainViewModel.PlayListsViewModel;
            this.PlayerEngine = App.Container.Resolve<PlayerEngine>();
            this.PlayerSettings = App.Container.Resolve<PlayerSettings>();

            this.PlayerEngine.PlayNextFileAction = () => {
                var playerMustBeStoped = !this.CanPlayNext();
                if (!playerMustBeStoped)
                {
                    playerMustBeStoped = !this.PlayerSettings.PlayerEngine.ShuffleMode
                                         && !this.PlayerSettings.PlayerEngine.RepeatMode
                                         && this.playListsViewModel.IsLastPlayListFile();
                    if (!playerMustBeStoped)
                    {
                        this.PlayNext();
                    }
                }
                if (playerMustBeStoped)
                {
                    this.Stop();
                }
            };

            var playerInitialized = this.WhenAnyValue(x => x.PlayerEngine.Initializied);

            this.ShuffleCommand = ReactiveCommand.Create(playerInitialized);
            this.ShuffleCommand.Subscribe(x => {
                this.PlayerSettings.PlayerEngine.ShuffleMode = !this.PlayerSettings.PlayerEngine.ShuffleMode;
            });

            this.RepeatCommand = ReactiveCommand.Create(playerInitialized);
            this.RepeatCommand.Subscribe(x => {
                this.PlayerSettings.PlayerEngine.RepeatMode = !this.PlayerSettings.PlayerEngine.RepeatMode;
            });

            this.MuteCommand = ReactiveCommand.Create(playerInitialized);
            this.MuteCommand.Subscribe(x => {
                this.PlayerEngine.IsMute = !this.PlayerEngine.IsMute;
            });
        }

        public PlayerEngine PlayerEngine { get; private set; }

        public PlayerSettings PlayerSettings { get; private set; }

        public ICommand PlayOrPauseCommand
        {
            get { return this.playOrPauseCommand ?? (this.playOrPauseCommand = new DelegateCommand(this.PlayOrPause, this.CanPlayOrPause)); }
        }

        private bool CanPlayOrPause()
        {
            return this.PlayerEngine.Initializied
                   && this.playListsViewModel.FirstSimplePlaylistFiles != null
                   && this.playListsViewModel.FirstSimplePlaylistFiles.OfType<IMediaFile>().Any();
        }

        private void PlayOrPause()
        {
            if (this.PlayerEngine.State == PlayerState.Pause || this.PlayerEngine.State == PlayerState.Play)
            {
                this.PlayerEngine.Pause();
            }
            else
            {
                var file = this.playListsViewModel.GetCurrentPlayListFile();
                if (file != null)
                {
                    this.PlayerEngine.Play(file);
                }
            }
        }

        public ICommand StopCommand
        {
            get { return this.stopCommand ?? (this.stopCommand = new DelegateCommand(this.Stop, this.CanStop)); }
        }

        private bool CanStop()
        {
            return this.PlayerEngine.Initializied
                   && this.playListsViewModel.FirstSimplePlaylistFiles != null
                   && this.playListsViewModel.FirstSimplePlaylistFiles.OfType<IMediaFile>().Any();
        }

        private void Stop()
        {
            this.PlayerEngine.Stop();
            // at this we should re-set the current playlist item and the handsome selected file
            this.playListsViewModel.ResetCurrentItemAndSelection();
        }

        public ICommand PlayPrevCommand
        {
            get { return this.playPrevCommand ?? (this.playPrevCommand = new DelegateCommand(this.PlayPrev, this.CanPlayPrev)); }
        }

        private bool CanPlayPrev()
        {
            return this.PlayerEngine.Initializied
                   && this.playListsViewModel.FirstSimplePlaylistFiles != null
                   && this.playListsViewModel.FirstSimplePlaylistFiles.OfType<IMediaFile>().Any();
        }

        private void PlayPrev()
        {
            var file = this.playListsViewModel.GetPrevPlayListFile();
            if (file != null)
            {
                this.PlayerEngine.Play(file);
            }
        }

        public ICommand PlayNextCommand
        {
            get { return this.playNextCommand ?? (this.playNextCommand = new DelegateCommand(this.PlayNext, this.CanPlayNext)); }
        }

        private bool CanPlayNext()
        {
            return this.PlayerEngine.Initializied
                   && this.playListsViewModel.FirstSimplePlaylistFiles != null
                   && this.playListsViewModel.FirstSimplePlaylistFiles.OfType<IMediaFile>().Any();
        }

        private void PlayNext()
        {
            var file = this.playListsViewModel.GetNextPlayListFile();
            if (file != null)
            {
                this.PlayerEngine.Play(file);
            }
        }

        public ReactiveCommand<object> ShuffleCommand { get; private set; }

        public ReactiveCommand<object> RepeatCommand { get; private set; }

        public ReactiveCommand<object> MuteCommand { get; private set; }

        public ReactiveCommand<object> ShowMediaLibraryCommand { get; private set; }

        public ReactiveCommand<Unit> ShowEqualizerCommand { get; private set; }
    }
}