﻿using ReactiveUI;

namespace Tunebook.ViewModels
{
    public class PlayControlInfoViewModel : ReactiveObject
    {
        public PlayControlViewModel PlayControlViewModel { get; private set; }

        public PlayControlInfoViewModel(MainViewModel mainViewModel)
        {
            this.PlayControlViewModel = new PlayControlViewModel(mainViewModel);
        }
    }
}
