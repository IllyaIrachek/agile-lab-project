﻿using System.Linq;
using System.Windows;
using ReactiveUI;
using Tunebook.Core.Player;
using Unity;

namespace Tunebook.ViewModels
{
    public class MainViewModel : ReactiveObject
    {
        public PlayerEngine PlayerEngine { get; private set; }

        public PlayerSettings PlayerSettings { get; private set; }

        public PlayControlInfoViewModel PlayControlInfoViewModel { get; private set; }

        public PlayListsViewModel PlayListsViewModel { get; private set; }

        public MainViewModel()
        {
            this.PlayerSettings = App.Container.Resolve<PlayerSettings>().Update();

            this.PlayerEngine = App.Container.Resolve<PlayerEngine>().Configure();

            this.PlayListsViewModel = new PlayListsViewModel();

            this.PlayControlInfoViewModel = new PlayControlInfoViewModel(this);

        }

        public void ShutDown()
        {
            foreach (var w in Application.Current.Windows.OfType<Window>())
            {
                w.Close();
            }
            this.PlayerSettings.Save();
            this.PlayerEngine.CleanUp();
            this.PlayListsViewModel.SavePlayList();
        }
    }
}